package com.simianempire.robot.catalogue.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.simianempire.robot.catalogue.api.Part;
import com.simianempire.robot.catalogue.jdbi.PartDAO;

public class PartsResourceTest {

    private static PartDAO partDAO = null;
    private static List<Part> parts = new ArrayList<Part>();
    private static Part partA = new Part("Part A", "PARTA", "Manufacturer A", 11.0f, null);
    private static Part partANoList = new Part("Part A", "PARTA", "Manufacturer A", 11.0f, null);
    private static Part partB = new Part("Part B", "PARTB", "Manufacturer B", 1.2f, null);
    private static List<String> compListA = new ArrayList<String>();
    private static List<String> compListAMinus = new ArrayList<String>();
    private static List<String> compListB = new ArrayList<String>();
    
    
    @BeforeClass
    public static void beforeClass(){
        partDAO = mock(PartDAO.class);      
        compListA.add("PARTA");//part A can be compatible with self
        compListA.add("PARTB");
        compListAMinus.add("PARTB");//for tests ensuring PK is not violated
        compListB.add("PARTA");     
        partA.setCompatibleSerialNumbers(compListA);
        partB.setCompatibleSerialNumbers(compListB);
        parts.add(partA);
        parts.add(partB);
    }
    
    
    public void happySetup(){       
        when(partDAO.selectAllParts()).thenReturn(parts);
        when(partDAO.selectPartBySerialNumber(eq("PARTA"))).thenReturn(partANoList);
        when(partDAO.deletePart(any(String.class))).thenReturn(1);
        when(partDAO.insertPart(any(Part.class))).thenReturn(1);
        when(partDAO.updatePart(eq("PARTA"), eq(partA))).thenReturn(1);
        when(partDAO.updatePart(eq("PARTB"), eq(partB))).thenReturn(0); // simulate updating a part that doesn't exist.
        when(partDAO.selectCompatibleParts(eq("PARTA"))).thenReturn(parts);
        when(partDAO.removeAllCompatiblePartsLeft(eq("PARTA"))).thenReturn(2);
        when(partDAO.removeAllCompatiblePartsRight(eq("PARTA"))).thenReturn(2);
        when(partDAO.addCompatiblePartLeft(eq("PARTA"), eq(compListA))).thenReturn(new int[]{1,1});
        when(partDAO.addCompatiblePartRight(eq("PARTA"), eq(compListAMinus))).thenReturn(new int[]{1});   
        when(partDAO.addCompatiblePartLeft(eq("PARTB"), eq(compListB))).thenReturn(new int[]{1});
        when(partDAO.addCompatiblePartRight(eq("PARTB"), eq(compListB))).thenReturn(new int[]{1});      
    }
    
    @After
    public void teardown(){
        reset(partDAO);
    }

//////Test Happy Path
    @Test
    public void getList(){
        happySetup();
        PartsResource resource = new PartsResource(partDAO);        
        List<Part> partsList = resource.list();
        assertThat(partsList).isEqualTo(parts);
        verify(partDAO).selectAllParts();
    }
    
    @Test
    public void getPart(){
        happySetup();
        PartsResource resource = new PartsResource(partDAO);        
        Part part = resource.read("PARTA");     
        assertThat(part).isEqualTo(partA);
        verify(partDAO).selectPartBySerialNumber("PARTA");
        verify(partDAO).selectCompatibleParts("PARTA");
    }
    
    @Test
    public void addPart(){
        happySetup();
        PartsResource resource = new PartsResource(partDAO);        
        Response response = resource.add(partA);
        assertThat(response.getStatus()).isEqualTo(Status.CREATED.getStatusCode());
        verify(partDAO).begin();
        verify(partDAO).addCompatiblePartLeft("PARTA", compListA);
        verify(partDAO).addCompatiblePartRight("PARTA", compListAMinus);
        verify(partDAO).insertPart(partA);
        verify(partDAO).commit();
    }
    
    @Test
    public void updatePart(){
        happySetup();
        PartsResource resource = new PartsResource(partDAO);        
        Response response = resource.update("PARTA", partA);        
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());
        verify(partDAO).begin();
        verify(partDAO).removeAllCompatiblePartsLeft("PARTA");
        verify(partDAO).removeAllCompatiblePartsRight("PARTA");
        verify(partDAO).addCompatiblePartLeft("PARTA", compListA);
        verify(partDAO).addCompatiblePartRight("PARTA", compListAMinus);
        verify(partDAO).updatePart("PARTA", partA);
        verify(partDAO).commit();
    }
    
    @Test
    public void deletePart(){
        happySetup();
        PartsResource resource = new PartsResource(partDAO);        
        Response response = resource.delete("PARTA");       
        assertThat(response.getStatus()).isEqualTo(Status.OK.getStatusCode());  
        verify(partDAO).begin();
        verify(partDAO).removeAllCompatiblePartsLeft("PARTA");
        verify(partDAO).removeAllCompatiblePartsRight("PARTA");
        verify(partDAO).deletePart("PARTA");
        verify(partDAO).commit();
    }
    
//////Test Data issues
    @Test
    public void getUnknownPart(){
        PartsResource resource = new PartsResource(partDAO);
        WebApplicationException theException = null;
        Part part = null;
        try{
            part = resource.read("PARTC");
        }
        catch (WebApplicationException e){
            theException = e;
        }
        assertThat(part).isNull();
        assertThat(theException.getResponse().getStatus()).isEqualTo(Status.NOT_FOUND.getStatusCode());
        verify(partDAO).selectPartBySerialNumber("PARTC");
    }
    
    /**
     * Update backs off to add 
     */
    @Test
    public void updateUnknownPart(){
        happySetup();
        PartsResource resource = new PartsResource(partDAO);
        WebApplicationException theException = null;
        Response response= null;
        try{
            response = resource.update(partB.getSerialNumber(), partB);
        }
        catch (WebApplicationException e){
            theException = e;
        }
        assertThat(response.getStatus()).isEqualTo(Status.CREATED.getStatusCode()); 
        assertThat(theException).isNull();

        verify(partDAO, times(2)).begin();
        verify(partDAO).rollback();
        verify(partDAO).updatePart("PARTB", partB);
        verify(partDAO).insertPart(partB);
        verify(partDAO).removeAllCompatiblePartsLeft(any(String.class));
        verify(partDAO).removeAllCompatiblePartsRight(any(String.class));
        verify(partDAO).addCompatiblePartLeft("PARTB", compListB);
        verify(partDAO).addCompatiblePartRight("PARTB", compListB);
        verify(partDAO).updatePart("PARTB", partB);
        verify(partDAO).commit();
    }
    
    @Test
    public void didNotDelete(){
        PartsResource resource = new PartsResource(partDAO);
        WebApplicationException theException = null;
        Response response = null;
        try{
            response = resource.delete("PARTC");
        }
        catch (WebApplicationException e){
            theException = e;
        }
        assertThat(response).isNull();
        assertThat(theException.getResponse().getStatus()).isEqualTo(Status.NOT_FOUND.getStatusCode());
        verify(partDAO).begin();
        verify(partDAO).deletePart("PARTC");
        verify(partDAO, never()).removeAllCompatiblePartsLeft(any(String.class));
        verify(partDAO, never()).removeAllCompatiblePartsRight(any(String.class));
        verify(partDAO, never()).commit();
        verify(partDAO).rollback();     
        
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void didNotAdd(){
        PartsResource resource = new PartsResource(partDAO);
        WebApplicationException theException = null;
        Response response = null;
        try{
            response = resource.add(partA);
        }
        catch (WebApplicationException e){
            theException = e;
        }
        assertThat(response).isNull();
        assertThat(theException.getResponse().getStatus()).isEqualTo(Status.INTERNAL_SERVER_ERROR.getStatusCode());
        verify(partDAO).begin();
        verify(partDAO).insertPart(partA);
        verify(partDAO, never()).addCompatiblePartLeft(any(String.class), any(List.class));
        verify(partDAO, never()).addCompatiblePartRight(any(String.class), any(List.class));
        verify(partDAO, never()).commit();
        verify(partDAO).rollback();     
        
    }

    @SuppressWarnings("unchecked")
    @Test
    public void addCompatiblePartFailure(){
        when(partDAO.addCompatiblePartLeft(any(String.class), any(List.class))).thenReturn(new int[]{0,1});
        PartsResource resource = new PartsResource(partDAO);
        WebApplicationException theException = null;
        try{
            resource.addCompatibleParts(partA);
        }
        catch (WebApplicationException e){
            theException = e;
        }
        assertThat(theException.getResponse().getStatus()).isEqualTo(Status.BAD_REQUEST.getStatusCode());
        verify(partDAO).addCompatiblePartLeft(any(String.class), any(List.class));
        verify(partDAO, never()).addCompatiblePartRight(any(String.class), any(List.class));
    }
    
}
