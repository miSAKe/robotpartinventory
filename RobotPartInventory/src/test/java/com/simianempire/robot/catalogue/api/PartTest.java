package com.simianempire.robot.catalogue.api;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.dropwizard.jackson.Jackson;

public class PartTest {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {
        final List<String> serialNumbers = new ArrayList<String>();
        serialNumbers.add("A");
        serialNumbers.add("B");
        final Part part = new Part("Test Part", "Test Serial Number", "Test Manufacturer", 1.5f, serialNumbers);

        final String expected = MAPPER.writeValueAsString(MAPPER.readValue(fixture("fixtures/part.json"), Part.class));

        assertThat(MAPPER.writeValueAsString(part)).isEqualTo(expected);
    }
    
    @Test
    public void deserializesFromJSON() throws Exception {
        final List<String> serialNumbers = new ArrayList<String>();
        serialNumbers.add("A");
        serialNumbers.add("B");
        final Part part = new Part("Test Part", "Test Serial Number", "Test Manufacturer", 1.5f, serialNumbers);
        assertThat(MAPPER.readValue(fixture("fixtures/part.json"), Part.class)).isEqualTo(part);
    }
    
    @Test
    public void testEqualsAndHash() {
        final Part partA = new Part("Test Part", "Test Serial Number", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Part partB = new Part("Test Part", "Test Serial Number", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Part partC = new Part("Test Part", "Test Serial Number3", "Test Manufacturer", 1.5f, new ArrayList<String>());

        assertTrue(partA.equals(partB));
        assertThat(partA.equals(partB)).isEqualTo(partB.equals(partA));
        assertFalse(partA.equals(partC));
        assertThat(partA.equals(partC)).isEqualTo(partC.equals(partA));
        assertThat(partA.hashCode()).isEqualTo(partB.hashCode());
    }
       
}

