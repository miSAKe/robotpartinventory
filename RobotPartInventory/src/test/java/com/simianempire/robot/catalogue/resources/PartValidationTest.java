package com.simianempire.robot.catalogue.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.ClassRule;
import org.junit.Test;

import com.simianempire.robot.catalogue.api.Part;
import com.simianempire.robot.catalogue.jdbi.PartDAO;

import io.dropwizard.jersey.validation.ValidationErrorMessage;
import io.dropwizard.testing.junit.ResourceTestRule;

/**
 * Test Validation Rules via Resource access
 * @author miSAKe
 *
 */
public class PartValidationTest {
    

    private static final PartDAO partDAO = mock(PartDAO.class); 

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder().addResource(new PartsResource(partDAO)).build();

    
    @Test
    public void nameMayNotBeNull() {
        Part badPart = new Part(null, "Serial", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("name may not be null");
    }

    
    @Test
    public void nameMayNotBeEmpty() {
        Part badPart = new Part("", "Serial", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("name size must be between 1 and 30");
    }
    
    @Test
    public void nameMayNotBeLong() {
        Part badPart = new Part("0123456789012345678901234567890", "Serial", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("name size must be between 1 and 30");
    }
    
    @Test
    public void snMayNotBeNull() {
        Part badPart = new Part("Test Name", null, "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("serialNumber may not be null");
    }

    
    @Test
    public void snMayNotBeEmpty() {
        Part badPart = new Part("Test Name", "", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("serialNumber size must be between 1 and 10");
    }
    
    @Test
    public void snMayNotBeLong() {
        Part badPart = new Part("Test Name", "01234567890", "Test Manufacturer", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("serialNumber size must be between 1 and 10");
    }
    
    @Test
    public void manufacturerMayNotBeNull() {
        Part badPart = new Part("Test Name", "Serial", null, 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("manufacturer may not be null");
    }

    
    @Test
    public void manufacturerMayNotBeEmpty() {
        Part badPart = new Part("Test Name", "Serial", "", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("manufacturer size must be between 1 and 30");
    }
    
    @Test
    public void manufacturerMayNotBeLong() {
        Part badPart = new Part("Test Name", "Serial", "0123456789012345678901234567890", 1.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("manufacturer size must be between 1 and 30");
    }
    
    @Test
    public void weightMayNotBeNull() {
        Part badPart = new Part("Test Name", "Serial", "Test Manufacturer", null, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("weight may not be null");
    }

    
    @Test
    public void weightMayNotBeBig() {
        Part badPart = new Part("Test Name", "Serial", "Test Manufacturer", 1234567.5f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("weight numeric value out of bounds (<6 digits>.<2 digits> expected)");
    }
    
    @Test
    public void weightMayNotBeAccurate() {
        Part badPart = new Part("Test Name", "Serial", "Test Manufacturer", 1.123f, new ArrayList<String>());
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("weight numeric value out of bounds (<6 digits>.<2 digits> expected)");
    }
    
    @Test
    public void listMayNotBeNull() {
        Part badPart = new Part("Test Name", "Serial", "Test Manufacturer", 1.5f, null);
        final Response post = resources.client().target("/parts").request().post(Entity.json(badPart));

        // Clients will recieve a 422 on bad request entity
        assertThat(post.getStatus()).isEqualTo(422);

        // Check to make sure that errors are correct and human readable
        ValidationErrorMessage msg = post.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors()).containsOnly("compatibleSerialNumbers may not be null");
    }
}
