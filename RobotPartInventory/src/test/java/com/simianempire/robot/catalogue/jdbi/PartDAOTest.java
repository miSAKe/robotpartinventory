package com.simianempire.robot.catalogue.jdbi;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.simianempire.robot.catalogue.api.Part;

/**
 * In memory database to test SQL
 * @author miSAKe
 *
 */
public class PartDAOTest {
    
    private static Handle handle;
    private static PartDAO partDAO;        
    
    private static Part partA = new Part("Part A", "PARTA", "Manufacturer A", 11.0f, null);
    private static Part partB = new Part("Part B", "PARTB", "Manufacturer B", 1.2f, null);
    private static List<String> compListA = new ArrayList<String>();
    private static List<String> compListB = new ArrayList<String>();
    
    
    /**
     * Open database connection and
     * build some test data
     */
    @BeforeClass
    public static void setup(){

        DataSource ds = JdbcConnectionPool.create("jdbc:h2:mem:test","username","password");
        DBI dbi = new DBI(ds);
        handle = dbi.open();
        partDAO = dbi.open(PartDAO.class);
        
        compListA.add("PARTA");//part A can be compatible with self
        compListA.add("PARTB");
        compListB.add("PARTA");     
    }   
    
    /**
     * Close DB Connections
     * @throws IOException 
     */
    @AfterClass
    public static void teardown() throws IOException{
        handle.close();
        partDAO.close();
    }
    
    /**
     * Create fresh database tables for each test
     * check that no exceptions were thrown.
     */
    @Before
    public void createTables(){
        Exception theException = null;
        try{
            partDAO.createPartsTable();
            partDAO.createCompatiblePartsTable();
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }
    
    /**
     * Destroy database after every test
     * check that no exceptions were thrown.
     */
    @After
    public void destroyTables(){
        Exception theException = null;
        try{
            handle.execute("drop table parts;");
            handle.execute("drop table compatible_parts;");
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }
    
    @Test
    public void testCreateTablesTwice(){
        Exception theException = null;
        try{
            partDAO.createPartsTable();
            partDAO.createCompatiblePartsTable();
        }
        catch (Exception e) {
            theException = e;
        }

        assertThat(theException).isNull();
    }
    
    
    @Test
    public void addInvalidPart(){
        int modified = 0;
        Exception theException = null;
        try{
            modified = partDAO.insertPart(new Part(null, null, null, null, null));
        }
        catch (Exception e) {
            theException = e;
        }

        assertThat(modified).isEqualTo(0);
        assertThat(theException.getMessage()).containsIgnoringCase("null not allowed");
    }
    
    @Test
    public void addAndRead(){
        Part readPart = null;
        int modified;
        Exception theException = null;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            readPart = partDAO.selectPartBySerialNumber("PARTA");
            assertThat(readPart).isEqualTo(partA);
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }

    @Test
    public void partsPKViolation(){
        Exception theException = null;
        int modified = 0;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified=0;
            modified = partDAO.insertPart(partA);
        }
        catch (Exception e) {
            theException = e;
        }
        assertThat(modified).isEqualTo(0);
        assertThat(theException).isNotNull();
        assertThat(theException.getMessage()).containsIgnoringCase("primary key violation");
    }
    
    @Test
    public void readUnknownPart(){
        Part readPart = null;
        Exception theException = null;
        try{
            readPart = partDAO.selectPartBySerialNumber("PARTA");
            assertThat(readPart).isNull();
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }
    
    @Test
    public void deletePart(){
        Part readPart = null;
        int modified;
        Exception theException = null;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            readPart = partDAO.selectPartBySerialNumber("PARTA");
            assertThat(readPart).isEqualTo(partA);
            modified = partDAO.deletePart("PARTA");
            assertThat(modified).isEqualTo(1);
            readPart = partDAO.selectPartBySerialNumber("PARTA");
            assertThat(readPart).isNull();
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }

    @Test
    public void deleteUnknownPart(){
        int modified;
        Exception theException = null;
        try{
            modified = partDAO.deletePart("PARTA");
            assertThat(modified).isEqualTo(0);
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }
    
    @Test
    public void updatePart(){
        Part readPart = null;
        int modified;
        Exception theException = null;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            readPart = partDAO.selectPartBySerialNumber("PARTA");
            assertThat(readPart).isEqualTo(partA);
            modified = partDAO.updatePart("PARTA", partB);
            assertThat(modified).isEqualTo(1);
            readPart = partDAO.selectPartBySerialNumber("PARTA");
            assertThat(readPart).isNull();
            readPart = partDAO.selectPartBySerialNumber("PARTB");
            assertThat(readPart).isEqualTo(partB);
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }
    
    @Test
    public void updateUnknownPart(){
        int modified;
        Exception theException = null;
        try{
            modified = partDAO.updatePart("PARTA", partB);
            assertThat(modified).isEqualTo(0);
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();
    }
    
    @Test
    public void selectAllNone(){
        List<Part> parts = null;
        Exception theException = null;
        try{
            parts = partDAO.selectAllParts();
            assertThat(parts).isEmpty();
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();      
    }
    
    @Test
    public void sellectAll(){
        List<Part> parts = null;
        Exception theException = null;
        int modified;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified = partDAO.insertPart(partB);
            assertThat(modified).isEqualTo(1);
            parts = partDAO.selectAllParts();
            assertThat(parts).contains(partA, partB);
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();      
    }
    
    @Test
    public void addCompatiblePartsLeft(){
        List<Part> parts = null;
        Exception theException = null;
        int modified;
        int[] inserts;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified = partDAO.insertPart(partB);
            assertThat(modified).isEqualTo(1);
            inserts = partDAO.addCompatiblePartLeft("PARTA", compListA);
            assertThat(inserts).containsExactly(1,1);
            parts = partDAO.selectCompatibleParts("PARTA");
            assertThat(parts).contains(partA, partB); // A is compatible with A & B
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();      
    }
    
    @Test
    public void addCompatiblePartsRight(){
        List<Part> parts = null;
        Exception theException = null;
        int modified;
        int[] inserts;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified = partDAO.insertPart(partB);
            assertThat(modified).isEqualTo(1);
            inserts = partDAO.addCompatiblePartRight("PARTA", compListA);
            assertThat(inserts).containsExactly(1,1);
            parts = partDAO.selectCompatibleParts("PARTB");
            assertThat(parts).contains(partA); // B is compatible with A only
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();      
    }
    
    @Test
    public void violateCompatiblePartsPK(){
        Exception theException = null;
        int modified;
        int[] inserts = null;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified = partDAO.insertPart(partB);
            assertThat(modified).isEqualTo(1);
            inserts = partDAO.addCompatiblePartLeft("PARTA", compListA);
            assertThat(inserts).containsExactly(1,1);
            inserts = null;
            inserts = partDAO.addCompatiblePartRight("PARTA", compListA); // because A is compatible with A it should violate PK
        }
        catch (Exception e) {
            theException = e;
        }
        assertThat(inserts).isNull();
        assertThat(theException).isNotNull();
        assertThat(theException.getMessage()).containsIgnoringCase("primary key violation");
    }
    
    @Test
    public void persistEmptyList(){
        Exception theException = null;
        int[] inserts = null;
        try{
            inserts = partDAO.addCompatiblePartLeft("PARTA", new ArrayList<String>());
            assertThat(inserts).isEmpty();
            inserts = null;
            inserts = partDAO.addCompatiblePartRight("PARTA", new ArrayList<String>());
            assertThat(inserts).isEmpty();
        }
        catch (Exception e) {
            theException = e;
        }
        assertThat(theException).isNull();
    }
    
    @Test
    public void removeCompatiblePartsLeft(){
        List<Part> parts = null;
        Exception theException = null;
        int modified;
        int[] inserts;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified = partDAO.insertPart(partB);
            assertThat(modified).isEqualTo(1);
            inserts = partDAO.addCompatiblePartLeft("PARTA", compListA);
            assertThat(inserts).containsExactly(1,1);
            parts = partDAO.selectCompatibleParts("PARTA");
            assertThat(parts).contains(partA, partB); // A is compatible with A & B
            modified = partDAO.removeAllCompatiblePartsLeft("PARTA");
            parts = partDAO.selectCompatibleParts("PARTA");
            assertThat(parts).isEmpty(); // A is compatible with nothing
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();              
    }
    
    @Test
    public void removeCompatiblePartsRight(){
        List<Part> parts = null;
        Exception theException = null;
        int modified;
        int[] inserts;
        try{
            modified = partDAO.insertPart(partA);
            assertThat(modified).isEqualTo(1);
            modified = partDAO.insertPart(partB);
            assertThat(modified).isEqualTo(1);
            inserts = partDAO.addCompatiblePartRight("PARTA", compListA);
            assertThat(inserts).containsExactly(1,1);
            parts = partDAO.selectCompatibleParts("PARTB");
            assertThat(parts).contains(partA); // B is compatible with A only
            modified = partDAO.removeAllCompatiblePartsRight("PARTA");
            parts = partDAO.selectCompatibleParts("PARTB");
            assertThat(parts).isEmpty(); // B is compatible with nothing
        }
        catch (Exception e) {
            theException = e;
        }
        
        assertThat(theException).isNull();              
    }
}
