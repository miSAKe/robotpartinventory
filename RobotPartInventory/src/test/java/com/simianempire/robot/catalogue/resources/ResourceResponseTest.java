package com.simianempire.robot.catalogue.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import com.simianempire.robot.catalogue.api.Part;
import com.simianempire.robot.catalogue.jdbi.PartDAO;

import io.dropwizard.testing.junit.ResourceTestRule;

/**
 * Test Happy Path Responses
 * @author miSAKe
 *
 */
public class ResourceResponseTest {
    

    private static final PartDAO partDAO = mock(PartDAO.class); 

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder().addResource(new PartsResource(partDAO)).build();
    
    @After
    public void reset(){
        Mockito.reset(partDAO);
    }

    /**
     * Expect OK 200 and list
     */
    @Test
    public void listTest() {
        Part partA = new Part("Name", "SerialA", "Test Manufacturer", 1.5f, new ArrayList<String>());
        Part partB = new Part("Name", "SerialB", "Test Manufacturer", 1.5f, new ArrayList<String>());
        List<Part> parts = new ArrayList<Part>();
        parts.add(partA);
        parts.add(partB);
        
        when(partDAO.selectAllParts()).thenReturn(parts);
        final Response get = resources.client().target("/parts").request().get();

        assertThat(get.getStatus()).isEqualTo(200);

        @SuppressWarnings("unchecked")
        List<String> list = get.readEntity(List.class);
        assertThat(list.size()).isEqualTo(2);
    }
    
    /**
     * Expect OK 200 and object
     */
    @Test
    public void readTest() {
        Part part = new Part("Name", "Serial", "Test Manufacturer", 1.5f, new ArrayList<String>());
        
        when(partDAO.selectPartBySerialNumber(any(String.class))).thenReturn(part);
        final Response get = resources.client().target("/parts/123").request().get();

        assertThat(get.getStatus()).isEqualTo(200);

        Part returned = get.readEntity(Part.class);
        assertThat(returned).isEqualTo(part);
    }

    /**
     * Expect created 201 and url
     */
    @Test
    public void addTest() {
        Part part = new Part("Name", "Serial", "Test Manufacturer", 1.5f, new ArrayList<String>());
        
        when(partDAO.insertPart(any(Part.class))).thenReturn(1);
        final Response post = resources.client().target("/parts").request().post(Entity.json(part));

        assertThat(post.getStatus()).isEqualTo(201);

        URI location = post.getLocation();
        assertThat(location.toString()).endsWith("/parts/Serial");
    }
    
    /**
     * Expect OK 200
     */
    @Test
    public void updateTest() {
        Part part = new Part("Name", "Serial", "Test Manufacturer", 1.5f, new ArrayList<String>());
        when(partDAO.updatePart(any(String.class), any(Part.class))).thenReturn(1);
        final Response put = resources.client().target("/parts/123").request().put(Entity.json(part));

        assertThat(put.getStatus()).isEqualTo(200);
    }
    
    /**
     * Expect OK 200
     */
    @Test
    public void deleteTest() {

        when(partDAO.deletePart(any(String.class))).thenReturn(1);
        final Response delete = resources.client().target("/parts/123").request().delete();

        assertThat(delete.getStatus()).isEqualTo(200);
    }

    /**
     * Expect OK 200 and List
     */
    @Test
    public void listCompatibleTest() {
        Part partA = new Part("Name", "SerialA", "Test Manufacturer", 1.5f, new ArrayList<String>());
        Part partB = new Part("Name", "SerialB", "Test Manufacturer", 1.5f, new ArrayList<String>());
        List<Part> parts = new ArrayList<Part>();
        parts.add(partA);
        parts.add(partB);
        
        when(partDAO.selectCompatibleParts(any(String.class))).thenReturn(parts);
        final Response get = resources.client().target("/parts/123/compatible").request().get();

        assertThat(get.getStatus()).isEqualTo(200);

        @SuppressWarnings("unchecked")
        List<String> list = get.readEntity(List.class);
        assertThat(list.size()).isEqualTo(2);
    }

}
