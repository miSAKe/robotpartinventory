package com.simianempire.robot.catalogue.resources;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import com.google.common.collect.Lists;
import com.simianempire.robot.catalogue.api.Part;
import com.simianempire.robot.catalogue.jdbi.PartDAO;

/**
 * 
 * @author miSAKe
 *
 */
@Path("/parts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PartsResource {
    
    private final PartDAO partDAO;

    @Inject
    public PartsResource(PartDAO partDAO) {
        this.partDAO = partDAO;     
    }

    //TODO optional paging
    /**
     * GET a list of all parts
     * @return List of parts     
     */
    @GET
    public List<Part> list() {
        return partDAO.selectAllParts();
    }
    
    /**
     * GET a specific part
     * @param serialNumber - the serial number of the requested part
     * @return Part or 404
     */
    @GET
    @Path("/{serialNumber}")
    public Part read(@PathParam("serialNumber") String serialNumber) {
        Part part = partDAO.selectPartBySerialNumber(serialNumber);
        if(part == null)
        {
            throw new WebApplicationException(Status.NOT_FOUND);
        }
        //TODO improve fetching of compatible part list, this is not optimal
        List<Part> compatibleParts = listCompatible(serialNumber);
        List<String> compatibleSerialNumbers = new ArrayList<String>();
        for(Part compatiblePart: compatibleParts){
            compatibleSerialNumbers.add(compatiblePart.getSerialNumber());
        }
        part.setCompatibleSerialNumbers(compatibleSerialNumbers);
        return part;
    }
    
    /**
     * POST to add a new part
     * @param part - the new part
     * @return CREATED response
     */
    @POST
    public Response add(@Valid Part part) {
        partDAO.begin();
        try{
            int modified = partDAO.insertPart(part);
            if( modified != 1){
                throw new WebApplicationException("Unable to persist part", Status.INTERNAL_SERVER_ERROR);
            }
            addCompatibleParts(part);
            partDAO.commit();
        }
        catch(Exception e) {
            partDAO.rollback();
            throw e;
        }
        URI location = UriBuilder.fromResource(PartsResource.class).path(part.getSerialNumber()).build();
        return Response.created(location).build();      
    }
    
    /**
     * PUT Update an existing part
     * @param serialNumber - serial number of part to update, the new serial number may be different.
     * @param part - the updated part
     * @return - OK response or CREATED if the part did not already exist
     */
    @PUT
    @Path("/{serialNumber}")
    public Response update(@PathParam("serialNumber") String serialNumber, @Valid Part part) {partDAO.begin();
        try{
            int modified = partDAO.updatePart(serialNumber, part);
            if( modified != 1){
                //Does not exist cannot update
                partDAO.rollback();
                //add instead
                return add(part);
            }
            addCompatibleParts(part);
            partDAO.commit();
        }
        catch (Exception e) {
            partDAO.rollback();
            throw e;
        }
        return Response.ok().build();       
    }   
    
    /**
     * Add compatible parts helper method
     * @param part
     */
    protected void addCompatibleParts(Part part){
        //always remove parts, to avoid PK violations and dead entries
        //if updating, the serial number may have changed.
        //if adding, the part may have previously been added as compatible, since there are no FK constraints
        partDAO.removeAllCompatiblePartsLeft(part.getSerialNumber());
        partDAO.removeAllCompatiblePartsRight(part.getSerialNumber());
        if(part.getCompatibleSerialNumbers().size() > 0){
            int modified = 0;
            int[] inserts = partDAO.addCompatiblePartLeft(part.getSerialNumber(), part.getCompatibleSerialNumbers());
            for(int value : inserts){
                modified += value;
            }
            if(modified != part.getCompatibleSerialNumbers().size()){
                throw new WebApplicationException("Unable to persist all compatible serial numbers", Status.BAD_REQUEST);
            }
            modified = 0;
            //remove self from compatability list to avoid PK violation
            List<String> compatibleParts = Lists.newArrayList(part.getCompatibleSerialNumbers());
            compatibleParts.remove(part.getSerialNumber());
            inserts = partDAO.addCompatiblePartRight(part.getSerialNumber(), compatibleParts);
            for(int value : inserts){
                modified += value;
            }
            if(modified != compatibleParts.size()){
                throw new WebApplicationException("Unable to persist all compatible serial numbers", Status.BAD_REQUEST);
            }
        }
    }

    
    /**
     * DELETE a part from the datastore
     * @param serialNumber - the serial number of the part to delete
     * @return OK response
     */
    @DELETE
    @Path("/{serialNumber}")
    public Response delete(@PathParam("serialNumber") String serialNumber) {
        partDAO.begin();
        try{
            int modified = partDAO.deletePart(serialNumber);
            if( modified != 1){
                throw new WebApplicationException(Status.NOT_FOUND);
            }
            partDAO.removeAllCompatiblePartsLeft(serialNumber);
            partDAO.removeAllCompatiblePartsRight(serialNumber);
            partDAO.commit();
        }
        catch (Exception e) {
            partDAO.rollback();
            throw e;
        }
        return Response.ok().build();
    }
    
    //TODO optional paging
    /**
     * GET a list of parts compatible with a specific part
     * @param serialNumber - the serial number of the part that must be compatible
     * @return List of compatible parts
     */
    @GET
    @Path("/{serialNumber}/compatible")
    public List<Part> listCompatible(@PathParam("serialNumber") String serialNumber) {
        return partDAO.selectCompatibleParts(serialNumber);
    }   

}
