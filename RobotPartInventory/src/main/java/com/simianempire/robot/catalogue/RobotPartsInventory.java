package com.simianempire.robot.catalogue;

import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.simianempire.robot.catalogue.jdbi.PartDAO;
import com.simianempire.robot.catalogue.resources.PartsResource;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Robot Inventory web service
 * @author miSAKe
 *
 */
public class RobotPartsInventory extends Application<RobotPartsInventoryConfiguration> {

    private static final Logger logger = LoggerFactory.getLogger(RobotPartsInventory.class);
    
    /**
     * Entry point
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        new RobotPartsInventory().run(args);
    }

    @Override
    public String getName() {
        return "Robot Parts Inventory";
    }

    /**
     * Serve the simple client from root.
     * the api will be served as in configuration
     */
    @Override
    public void initialize(Bootstrap<RobotPartsInventoryConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
    }

    /**
     * Register all resources here. 
     * Called by parent.run(args) after initialisation
     */
    @Override
    public void run(RobotPartsInventoryConfiguration configuration, Environment environment) {

    	// the DBI factory will manage the database lifecycle and health checks
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "Database");
        final PartDAO partDAO = jdbi.onDemand(PartDAO.class);

        //using in memory database for prototype/testing, create tables now, 
        try{

            logger.info("Initialising database tables");
            partDAO.createPartsTable();
            partDAO.createCompatiblePartsTable();
        }
        catch(Exception e){
            //tables are only created if not existing, something else went wrong
            logger.error("Unable to initialise database tables");
            throw e;            
        }
        
        environment.jersey().register(new PartsResource(partDAO));
    }
}