package com.simianempire.robot.catalogue.jdbi;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.simianempire.robot.catalogue.api.Part;

/**
 * Map Resultset to robot part
 * @author miSAKe
 *
 */
public class PartMapper  implements ResultSetMapper<Part>{

	/**
	 * Map a resultset from the PARTS table to a Part
	 * @return the part
	 */
    public Part map(int index, ResultSet r, StatementContext ctx) throws SQLException
    {
        return new Part(r.getString("name"), r.getString("serial_number"), r.getString("manufacturer"), r.getFloat("weight"), null);
    }
}
