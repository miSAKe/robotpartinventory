package com.simianempire.robot.catalogue;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

/**
 * Configuration file, maps to YAML, extends standard Configuration to inherit standard options
 * @author miSAKe
 *
 */
public class RobotPartsInventoryConfiguration extends Configuration {
    /**
     * DataSourceFactory exposes all the database configuration options for the DB
     */
	@Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();
    
    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }
    
    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }
}