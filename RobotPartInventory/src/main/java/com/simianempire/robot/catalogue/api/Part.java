package com.simianempire.robot.catalogue.api;

import java.util.List;
import java.util.Objects;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Robot Part
 * @author miSAKe
 * 
 * Validation to ensure validity and compatability with database structure.
 *
 */
public class Part {
    
    @NotNull
    @Size(min = 1, max = 30)
    private String name;
    
    @NotNull
    @Size(min = 1, max = 10)
    private String serialNumber;
    
    @NotNull
    @Size(min = 1, max = 30)
    private String manufacturer;
    
    /**
     * weight in grams
     */
    @NotNull
    @Digits(integer=6, fraction=2)
    private Float weight;
    
    /**
     * TODO add validation to list elements.
     */
    @NotNull
    private List<String> compatibleSerialNumbers;
    
    @SuppressWarnings("unused")
    private Part() {
        // Jackson deserialization
    }

    public Part(String name, String serialNumber, String manufacturer, Float weight, List<String> compatibleSerialNumbers) {
        this.name = name;
        this.serialNumber = serialNumber;
        this.manufacturer = manufacturer;
        this.weight = weight;
        this.compatibleSerialNumbers = compatibleSerialNumbers;
    }

    /**
     * @return the name
     */
    @JsonProperty
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    @JsonProperty
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the serialNumber
     */
    @JsonProperty
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    @JsonProperty
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the manufacturer
     */
    @JsonProperty
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * @param manufacturer the manufacturer to set
     */
    @JsonProperty
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     * @return the weight
     */
    @JsonProperty
    public Float getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    @JsonProperty
    public void setWeight(Float weight) {
        this.weight = weight;
    }

    /**
     * @return the compatibleSerialNumbers
     */
    @JsonProperty
    public List<String> getCompatibleSerialNumbers() {
        return compatibleSerialNumbers;
    }

    /**
     * @param compatibleSerialNumbers the compatibleSerialNumbers to set
     */
    @JsonProperty
    public void setCompatibleSerialNumbers(List<String> compatibleSerialNumbers) {
        this.compatibleSerialNumbers = compatibleSerialNumbers;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, serialNumber, manufacturer, weight, compatibleSerialNumbers);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        
        if (obj == null) return false;
        
        if (getClass() != obj.getClass()) return false;
        
        Part other = (Part) obj;
        
        if (!Objects.equals(compatibleSerialNumbers,other.compatibleSerialNumbers)) return false;
        
        if (!Objects.equals(manufacturer, other.manufacturer)) return false; 
        
        if (!Objects.equals(name, other.name)) return false;
        
        if (!Objects.equals(serialNumber, other.serialNumber)) return false;
        
        if (!Objects.equals(weight, other.weight)) return false;
        
        return true;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Part [name=%s, serialNumber=%s, manufacturer=%s, weight=%f]", name, serialNumber, manufacturer, weight);
    }
}
