package com.simianempire.robot.catalogue.jdbi;

import java.io.IOException;
import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlBatch;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.simianempire.robot.catalogue.api.Part;

/**
 * Part DAO
 * @author miSAKe
 *
 */
@RegisterMapper(PartMapper.class)
public interface PartDAO extends Transactional<PartDAO> {
	
	/**
	 * Create PARTS database table if it does not exist
	 */
    @SqlUpdate("create table if not exists parts (name varchar(30), serial_number varchar(10) primary key, manufacturer varchar(30), weight float)")
    void createPartsTable();    

    /**
     * Create COMPATIBLE_PARTS table if it does not exist
     */
    @SqlUpdate("create table if not exists compatible_parts (serial_number_a varchar(10), serial_number_b varchar(10),PRIMARY KEY (serial_number_a, serial_number_b))")
    void createCompatiblePartsTable();

    /**
     * Insert a new Part into the PARTS table
     * @param part - the new part
     * @return number of rows modified
     */
    @SqlUpdate("insert into parts (name, serial_number, manufacturer, weight) values (:name, :serialNumber, :manufacturer, :weight)")
    int insertPart(@BindBean Part part);        

    /**
     * Update an existing Part in the PARTS table
     * @param serialNumber - the old part primary key
     * @param part - the updated part (PK may have changed)
     * @return number of rows modified
     */
    @SqlUpdate("update parts set name=:name, manufacturer=:manufacturer, weight=:weight, serial_number=:serialNumber where serial_number=:oldSerialNumber")
    int updatePart(@Bind("oldSerialNumber") String serialNumber, @BindBean Part part);  
    
    /**
     * Remove an existing Part from the PARTS table
     * @param serialNumber - the primary key of the part to remove
     * @return number of rows modified
     */
    @SqlUpdate("delete from parts where serial_number=:serialNumber")
    int deletePart(@Bind("serialNumber") String serialNumber);      
    
    /**
     * Remove all serial numbers marked as compatible with a part from the COMPATIBLE_PARTS table
     * @param serialNumber  - the primary key of the part to remove
     * @return number of rows modified
     */
    @SqlUpdate("delete from compatible_parts where serial_number_a=:serialNumber")
    int removeAllCompatiblePartsLeft(@Bind("serialNumber") String serialNumber);
    
    /**
     * Remove all serial numbers marked as having the past part as compatible from the COMPATIBLE_PARTS table
     * @param serialNumber  - the primary key of the part to remove
     * @return number of rows modified
     */
    @SqlUpdate("delete from compatible_parts where serial_number_b=:serialNumber")
    int removeAllCompatiblePartsRight(@Bind("serialNumber") String serialNumber);
    
    /**
     * Add a list of serial numbers as compatible with a part to the COMPATIBLE_PARTS table
     * Addition takes place as a batch within a single transaction
     * @param serialNumber - the primary key of the part
     * @param compatibleSerialNumbers - the list of compatible serial numbers
     * @return array of number of rows modified for each insert within batch
     */
    @SqlBatch("insert into compatible_parts (serial_number_a, serial_number_b) values (:serialNumber, :compatibleSerialNumber)")
    int[] addCompatiblePartLeft(@Bind("serialNumber") String serialNumber, @Bind("compatibleSerialNumber") List<String> compatibleSerialNumbers);    
    
    /**
     * Add a part as compatible with a list of serial numbers to the COMPATIBLE_PARTS table
     * Addition takes place as a batch within a single transaction
     * @param serialNumber - the primary key of the part
     * @param compatibleSerialNumbers - the list of compatible serial numbers
     * @return array of number of rows modified for each insert within batch
     */
    @SqlBatch("insert into compatible_parts (serial_number_a, serial_number_b) values (:compatibleSerialNumber, :serialNumber)")
    int[] addCompatiblePartRight(@Bind("serialNumber") String serialNumber, @Bind("compatibleSerialNumber") List<String> compatibleSerialNumbers);   

    /**
     * Fetch part details from the PARTS table based on the serial number
     * @param serialNumber - the primary key of the part
     * @return the part matching the serial number
     */
    @SqlQuery("select name, serial_number, manufacturer, weight from parts where serial_number=:serialNumber")
    Part selectPartBySerialNumber(@Bind("serialNumber") String serialNumber);
    
    /**
     * Fetch a list of all the parts stored in the PARTS table
     * @return a list of parts
     */
    @SqlQuery("select name, serial_number, manufacturer, weight from parts")
    List<Part> selectAllParts();
    
    /**
     * Fetch a list of parts from the PARTS table that are compatible with the specified part
     * @param serialNumber - the primary key of the part that the returned list must be compatible with
     * @return a list of compatible parts
     */
    @SqlQuery("select p.name, p.serial_number, p.manufacturer, p.weight from parts p inner join compatible_parts cp on p.serial_number=cp.serial_number_b where cp.serial_number_a=:serialNumber")
    List<Part> selectCompatibleParts(@Bind("serialNumber") String serialNumber);
        
    /**
     * close with no args is used to close the connection
     */
    void close() throws IOException;
}
