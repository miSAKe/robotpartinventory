# README #

Example RESTful web service built using DropWizard

### Example Web Service ###

* Java Server built using [DropWizard](http://www.dropwizard.io/0.9.1/docs/)
    * Using [Jetty](http://www.eclipse.org/jetty/), [Jersey](https://jersey.java.net/), [Jackson](https://github.com/FasterXML/jackson) [JDBI](http://jdbi.org/) and [Hibernate Validator](http://hibernate.org/validator/)
* HTML and Javascript Client build using [JQuery](https://jquery.com/), [Bootstrap](http://getbootstrap.com/) and [KnockoutJS](http://knockoutjs.com/)
* JDBC database datastore, [H2 Database Engine](http://www.h2database.com/html/main.html) is bundled

### Building ###

* The source code for the Robot Part Inventory application can be found in the *RobotPartInventory* sub-directory
* [Vagrantfile](https://www.vagrantup.com/) included for automatic build tool provisioning
    * The Vagrantfile can be found in the root directory of the repository
    * The root directory is automatically mounted as */vagrant* within the virtual machine
    * Use `vagrant up` to create and provision a development environment
    * Use `vagrant ssh` to create a ssh session into the development environment
    * Use `vagrant destroy` to destroy the development environment
* [Puppet](https://puppetlabs.com/) manifests included for build tool provisioning
    * The main Puppet manifest can be found under the *manifests* sub-directory
    * The individual Puppet modules can be found under the *modules* sub-directory
* [Gradle 2.9](https://gradle.org/) is used with the java plugin for building and dependency management
    * `gradle build` within the *RobotPartInventory* sub-directory will download dependencies, compile, test and build the project
* The Gradle [Shadow plugin](https://github.com/johnrengelman/shadow) has been used to generate a self-contained fat-jar
    * `gradle shadowJar` within the *RobotPartInventory* sub-directory will build the self-contained fat-jar

### Configuration ###

* *default.yaml* is provided as a functioning sample configuration file within the root of the *RobotPartInventory* sub-directory
    * The main web server is on port *8080*
    * The default database is H2 running in memory only (data is not persisted to disk when the application stops) see the [cheatsheet](http://www.h2database.com/html/cheatSheet.html) for other H2 modes
    * Logging is set to *INFO* and is to *console* only
    * The root path for resources is set to */api/*
* DropWizard wraps most options with sensible defaults, to view the defaults and available options please see the [configuration reference page](https://dropwizard.github.io/dropwizard/0.9.1/docs/manual/configuration.html)

### Running ###

* The server is started by passing the *server* parameter to the application along with the configuration *YAML* file to be used
    * e.g `java -jar RobotPartInventory-0.0.1-all.jar server default.yaml`
* The client works as a stand alone HTML page but is also served from the server.
    * The HTML page can be found as *RobotPartInventory/src/main/resources/assets/index.html*
    * The *index.html* is also hosted on the server root e.g [http://localhost:8080/]
* The end points are hosted at *api/parts* e.g [http://localhost:8080/api/parts]
    * GET     /api/parts                            returns a JSON array of parts
    * POST    /api/parts                            takes a JSON part
    * DELETE  /api/parts/{serialNumber}
    * GET     /api/parts/{serialNumber}             returns a JSON part
    * PUT     /api/parts/{serialNumber}             takes a JSON part
    * GET     /api/parts/{serialNumber}/compatible  returns a JSON array of parts
* A sample JSON part object can be found *RobotPartInventory/src/test/resources/fixtures/part.json*
* DropWizard also provides some operational services which can be accessed from port *8081* by default 
    * DropWizard also provides *tasks* endpoints for forcing garbage collection and changing the logging level
    
### TODO ###

 * Tooling
     * Add Gradle Wrapper, build environment is simple, Vagrant is probably not nessecary
     * Add creation of Docker container as build option
     * Update Puppet modules to only call *apt-get update* for specific repositories
 * Server
     * Seperate API package into seperate project to build a Java client API library
     * Move data store logic out of Resources and main class and into a cleaner data store abstraction
     * Add optional paging capability to get all parts resource and get compatible parts resource
     * Improve code that fetches lists of compatible serial numbers
     * Add validation to lists of compatible serial numbers passed as inputs to resources
     * Add foreign key constrains between tables and handle violations
     * Update DAO test to be parameterised to using H2's compatability modes to test for other databases
     * Add integration tests
 * Client
     * Split Javascript into its own file
     * Add user input validation
     * Add unit tests
     * Add integration tests
     * Simplify Javascript code, current rather verbose
     * Use Bootstrap Alerts
     * Resdesign compatible part support to use check boxes
     * Update tables to use paging
   
### Known Issues ###
 
 * The Gradle *eclipse* command from within the Vagrant session cannot create a classpath valid for use outside of the virtual machine
