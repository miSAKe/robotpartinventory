class java (
    $java_version = '8'
) 
{
    include python-software-properties
    
    # add repository with java installers   
    exec { 'add-apt-repository ppa:webupd8team/java': 
      command => '/usr/bin/add-apt-repository ppa:webupd8team/java',
    }

    exec { 'apt-get update java':
      command => '/usr/bin/apt-get update',
      require => Exec['add-apt-repository ppa:webupd8team/java'],
    }

    # accepting Oracle's licence before installing
    exec {
      "accept_license":
      command   => "echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections",
      cwd       => "/home/vagrant",
      user      => "vagrant",
      path      => "/usr/bin/:/bin/",
      before    => Package["oracle-java${java_version}-installer"],
      logoutput => true,
    }   
    
    package { "oracle-java${java_version}-installer":
      ensure => present,
      require => Exec["apt-get update java"],
    }
    
    package { "oracle-java${java_version}-set-default":
      ensure  => present,
      require => Package["oracle-java${java_version}-installer"],
    }    
}