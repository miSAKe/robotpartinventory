class python-software-properties ()
{
    # puppet seems to have some trouble if a version is not specified
    package { 'python-software-properties':
      ensure => '0.82.7',
    }
}