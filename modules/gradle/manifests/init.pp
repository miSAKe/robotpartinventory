class gradle (
    $gradle_version = '2.9'
)
{
    include python-software-properties
    include java

    # add repository with gradle installers   
    exec { 'add-apt-repository ppa:cwchien/gradle': 
      command => '/usr/bin/add-apt-repository ppa:cwchien/gradle',
    }

    exec { 'apt-get update gradle':
      command => '/usr/bin/apt-get update',
      require => Exec['add-apt-repository ppa:cwchien/gradle'],
    }
    
    package { "gradle-${gradle_version}":
      ensure => present,
      require => Exec["apt-get update gradle"],
    }
}